package test

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"github.com/labstack/echo"
	"gitlab.com/tamnt52/example/handler"

	. "gopkg.in/check.v1"
)

func Test(t *testing.T) { TestingT(t) }

type S struct {
	Server *echo.Echo
}

var _ = Suite(&S{})

func (s *S) SetUpSuite(c *C) {
	s.Server = echo.New()
	s.Server.GET("/healthcheck", handler.HealthCheck)
}

func (s *S) TearDownSuite(c *C) {
}

func (s *S) SetUpTest(c *C) {
}

func (s *S) TearDownTest(c *C) {
}

func (s *S) PerformRequest(method string, path string, params url.Values) *httptest.ResponseRecorder {
	paramsEncoded := params.Encode()
	reader := strings.NewReader(paramsEncoded)
	if method == "GET" || method == "HEAD" {
		path += "?" + paramsEncoded
	}

	request, err := http.NewRequest(method, path, reader)
	if err != nil {
		panic(err)
	}

	if method == "POST" || method == "PUT" {
		request.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	}

	response := httptest.NewRecorder()

	s.Server.ServeHTTP(response, request)
	return response
}

// TestHealthCheck is func test for handler HealthCheck
func (s *S) TestHealthCheck(c *C) {
	res := s.PerformRequest("GET", "/healthcheck", url.Values{})
	c.Assert(http.StatusOK, Equals, res.Code)
	c.Assert("true\n", Equals, res.Body.String())
}

// BenchmarkHealthCheck is func test benchmark of gocheck. You can use that go test -check.b -check.bmem
func (s *S) BenchmarkHealthCheck(c *C) {
	for i := 0; i < c.N; i++ {
		s.PerformRequest("GET", "/healthcheck", url.Values{})
	}
}

// vi:syntax=go
