FROM golang:1.11.4-stretch as builder
WORKDIR /go/src/gitlab.com/tamnt52/example/
COPY . /go/src/gitlab.com/tamnt52/example
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o ./dist/app
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o ./dist/health-check "gitlab.com/tamnt52/example/healthcheck"

FROM alpine:3.5
RUN apk add --update ca-certificates
RUN apk add --no-cache tzdata && \
  cp -f /usr/share/zoneinfo/Asia/Ho_Chi_Minh /etc/localtime && \
  apk del tzdata

WORKDIR /app
COPY --from=builder go/src/gitlab.com/tamnt52/example/dist/app .
COPY --from=builder go/src/gitlab.com/tamnt52/example/dist/health-check .

HEALTHCHECK --interval=5s --timeout=1s --start-period=2s --retries=10 CMD [ "./health-check" ]

ENV PORT=9090
EXPOSE $PORT
ENTRYPOINT ["./app"]
