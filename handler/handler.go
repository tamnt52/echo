package handler

import (
	"net/http"

	"github.com/labstack/echo"
)

// HealthCheck is func health check status service
// @Summary health check
// @Description health check status service
// @Accept  json
// @Produce  json
// @Success 200 {boolean} true
// @Failure 500 {object} errors
// @Router /healthcheck [get]
func HealthCheck(c echo.Context) error {
	// To do something
	return c.JSON(http.StatusOK, true)
}
