package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

func main() {
	httpResp, err := http.Get(fmt.Sprintf("http://127.0.0.1:%s/healthcheck", os.Getenv("PORT")))
	if err != nil {
		fmt.Println("health check error ", err.Error())
		os.Exit(1)
	}
	if httpResp.StatusCode != 200 {
		resp, err := ioutil.ReadAll(httpResp.Body)
		if err != nil {
			fmt.Println("read all data  ", err.Error())
		}
		fmt.Println("health check error: ", string(resp))
		os.Exit(1)
	}
}
