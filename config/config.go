package config

import (
	"fmt"
	"strings"

	"github.com/spf13/viper"
)

// Schema is struct
type Schema struct {
	Profiler struct {
		Prometheus    bool   `mapstructure:"prometheus"`
		StatsdAddress string `mapstructure:"statsd_address"`
		Service       string `mapstructure:"service"`
	} `mapstructure:"profiler"`
}

// Config is varible
var Config Schema

func init() {
	config := viper.New()
	config.SetConfigName("config")
	config.AddConfigPath(".")          // Look for config in current directory
	config.AddConfigPath("config/")    // Optionally look for config in the working directory.
	config.AddConfigPath("../config/") // Look for config needed for tests.
	config.AddConfigPath("../")        // Look for config needed for tests.

	config.SetEnvKeyReplacer(strings.NewReplacer(".", "__"))
	config.AutomaticEnv()

	err := config.ReadInConfig() // Find and read the config file
	if err != nil {              // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s ", err))
	}

	err = config.Unmarshal(&Config)
	if err != nil { // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s ", err))
	}
}

func mm() {

}
