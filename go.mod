module gitlab.com/tamnt52/example

go 1.13

require (
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/spf13/viper v1.4.0
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127
)
